

function addition(number1, number2){
	// return number1 + number2;
	// console.log(number1 + number2);
	console.log("Displayed sum of 5 and 15")
	console.log(number1 + number2)
}
addition(15, 5)




function subtraction(number1, number2){
	console.log("Displayed difference of 20 and 5")
	console.log(number1 - number2);
}
subtraction(20, 5)


function multiply(number1, number2){
	return number1 * number2
}
let product = multiply(50, 10);
console.log("The product of 50 and 10:")
console.log(product)



function divide(number1, number2){
	
	return number1 / number2;
}
let quotient = divide(50, 10)
console.log("The quotient of 50 and 10:")
console.log(quotient)


function totalAreaOfCircle(number1, number2){
	return (number1 * number1 ) * number2;
}
let circleArea = totalAreaOfCircle(15, Math.PI)
console.log("The result of getting the area of a circle with 15 radius:")
console.log(circleArea)


function average(number1, number2, number3, number4){
	return (number1 + number2 + number3 + number4) / 4;
}
let averageVar = average(20, 40, 60, 80);
console.log("The average of 20, 40, 60 and 80:")
console.log(averageVar)


function checkIfPassed(myScore, totalScore){
	let isPassed =(myScore/totalScore)*100 >=75;
	return isPassed
}

let isPassingScore = checkIfPassed(38, 50);
console.log('Is 38/50 a passing score?')
console.log(isPassingScore)
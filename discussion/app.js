
// "name" - is a parameter
//A parameter is a variable/container  that can only exsist in our function and is used to store information  that is provided to a funtion when it is called/invoked.
function printName(name){
	console.log("My name is " + name);
}

//Data passed into a function invocation can be recieved by the function
//This is what we call an argument
//"Juliana", "Jimin" are examples of an argument
printName("Juliana");
printName("Jimin");

//Data passed into the function through function invocation is called
//The argument is then stored within a container called a parameter.

function printMyAge(age){
	console.log("I am " + age);
}

printMyAge(25);
printMyAge();

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " is divisible by 8?");
	console.log(isDivisibleBy8)
}

checkDivisibilityBy8(40);
checkDivisibilityBy8(27);


function favouriteSuperhero(name){
	console.log('My favourite Superhero is ' + name);
}

favouriteSuperhero('DareDevil');

function printMyFavouriteLanguage(language){
	console.log("My favourite language is: " + language);
}

printMyFavouriteLanguage("Javascript");
printMyFavouriteLanguage("Java");

//Multiple Arguments can also be passed into a function; Multiple parameters can contain our arguments.

function printFullName(firstName, middleName, lastName){
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName("Juan", "A.", "Dela Cruz");
printFullName("Ibbara", "Juan", "Crisostomo");
printFullName("Dayhyun", "Kim", "Calinao");
printFullName("Dayhyun", "Kim Dela", "Cruz")

/*
	Parameters will contain the arguments according to the order list it was passed.


	In other languages, providing more/less arguments than the expected
	parameters sometimes causes an error or changes the behavior of the function.

	In Javascript, we don't  have to worry about that.

	In Javascript, providing less arguments than the expected parameters will automatically
	assign an undefined value to the parameter
*/


//User Varameters as Arguments
let fName = "Jayce";
let mName = "Ashe";
let lName = "Warwick";

printFullName(fName, mName, lName);

function favoriteSongs(faveSong1, faveSong2, faveSong3, faveSong4, faveSong5){
	console.log(`My favourite songs are: ${faveSong1}, ${faveSong2}, ${faveSong3}, ${faveSong4}, ${faveSong5}`);

	return "pogi ako"
}



let sample1 =favoriteSongs("Jesus of Suburbia", "Go!", "Budots", "Pokemon theme song", "Carry on Wayward Son")
console.log(sample1)

 
// Return statement

//currently or so far, our functions are able to display data in our console.
//However, our functions cannot yet return values. Functions are able to return values which can be saved into a variable using the return statement/keyword


let fullName = printFullName("Marco", "Joseph", "Lacdao");

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
}
fullName = returnFullName("Ernesto", "Antonio", "Policarpio")
console.log(fullName)

//function which have a return statement are able to return value and it can be save in a variable.

console.log(fullName + " is my grandpa.")


function returnPhilAddress(city){
	return city + ", Philipines"
	console.log(city)
}


//return - reutnr a value from a function which we can save in a variable
let myFullAdress = returnPhilAddress("Cainta")
console.log(myFullAdress)

function checkDivisibilityBy4(num){

	let remainder = num % 4;

	let isDivisibleBy4 = remainder === 0;

	//returns either true or false
	//Not only can you return raw values/data, you can also directly return a variable
	return isDivisibleBy4;
	//return keyword not only allows us to return value  but also ends the process of our function
	console.log("I am running after the return")
}

let num4IsDivisibleeBy4 = checkDivisibilityBy4(4);
let num14IsDivisibleeBy4 = checkDivisibilityBy4(14);

console.log(num4IsDivisibleeBy4);
console.log(num14IsDivisibleeBy4);


function createPlayerInfo(username, level, job){
	return `Username: ${username}, Level: ${level}, Job: ${job}`
}

let user1 = createPlayerInfo("white_night", 95, "Paladin");
console.log(user1)
